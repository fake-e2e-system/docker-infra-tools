FROM alpine
RUN mkdir /root/infra-tools
WORKDIR /root/infra-tools
ADD https://releases.hashicorp.com/packer/1.2.4/packer_1.2.4_linux_amd64.zip .
ADD https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip .
RUN unzip packer_1.2.4_linux_amd64.zip
RUN unzip terraform_0.11.7_linux_amd64.zip
RUN mv packer /usr/local/bin/packer
RUN mv terraform /usr/local/bin/terraform
RUN rm packer_1.2.4_linux_amd64.zip
RUN rm terraform_0.11.7_linux_amd64.zip
RUN apk update && \
 apk --no-cache add curl \
                    ansible \
                    groff \
                    less \
                    ca-certificates \
                    ruby-dev \
                    make \
                    g++ \
                    jq \
                    python \
                    ruby \
                    ruby-bigdecimal \
                    ruby-bundler \
                    ruby-io-console \
                    ruby-irb \
                    py-pip && \
  	pip install awscli && \
  	apk --purge -v del py-pip && \
    update-ca-certificates \
    rm -rf /tmp/* && \
  	rm /var/cache/apk/*
